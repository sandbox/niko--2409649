<?php
/**
 * A way2pay payment method controller.
 *
 * This controller always executes payments successfully, does not require
 * payment-specific information and does not transfer any money. It can be
 * useful to create a "collect on delivery" payment method, for instance.
 */
class PaymentMethodWayToPayController extends PaymentMethodController {

  public $controller_data_defaults = array(
    'message' => '',
    'status' => PAYMENT_STATUS_PENDING,
    'text_format' => '',
    'merchant_id' => '',
    'action_url' => 'https://waytopay.org/merchant/index',
    'secret_key' => '',
    'inc_curr' => 0,
  );

  static $payways = array(
    1 => 'WebMoney WMR',
    2 => 'WebMoney WMZ',
    3 => 'WebMoney WME',
    4 => 'WebMoney WMU',
    5 => 'SMS',
    6 => 'QIWI',
    8 => 'Z-Payment',
    15 => 'VISA / MasterCard',
    14 => 'Яндекс.Деньги',
    20 => 'TELE 2',
    17 => 'МТС',
    18 => 'Билайн',
    19 => 'Мегафон',
    22 => 'Евросеть',
  );

  public $payment_method_configuration_form_elements_callback = 'waytopay_payment_method_configuration_form_elements';

  public $payment_configuration_form_elements_callback = 'waytopay_payment_configuration_form_elements';

  function __construct() {
    $currency_codes = array('RUB');
    $this->currencies = array_fill_keys($currency_codes, array());
    $this->title = t('WAY to PAY');
    $this->description = t('WAY to PAY payment method type.');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(Payment $payment) {
    entity_save('payment', $payment);
    drupal_goto('waytopay/redirect/' . $payment->pid);
  }

  /**
   * Calc signature for $_POST data.
   *
   * @param PaymentMethod $payment_method
   * @param array $data
   *
   * @return string
   */
  static protected function signature(PaymentMethod $payment_method, array $data) {
    $signature_data = array(
      $payment_method->controller_data['merchant_id'],
      $data['wOutSum'],
      $data['wInvId'],
      $payment_method->controller_data['secret_key'],
    );
    $signature_string = implode(':', $signature_data);

    // Add additional shop fields to signature string
    $basic_keys = array('wOutSum','wInvId','wIsTest','wSignature');
    foreach ($data as $key => $value) {
      if (!in_array($key, $basic_keys)) {
        $signature_string .= ':' . $key . '=' . $value;
      }
    }

    return md5($signature_string);
  }

  /**
   * Validate $_POST data.
   *
   * @param array $data
   *
   * @return array
   */
  static public function validate_post(array $data) {
    $required_keys = array('wOutSum','wInvId','wIsTest','wSignature');
    $unavailable_required_keys = array_diff_key(array_flip($required_keys), $data);
    if (!empty($unavailable_required_keys)) {
      watchdog('WayToPay', 'Missing POST keys. $unavailable_required_keys: <pre>!data</pre>', array('!data' => print_r($unavailable_required_keys,TRUE)), WATCHDOG_WARNING);
      return array('type' => 'ERROR', 'result' => 'Missing POST keys.');
    }

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'payment')
      ->entityCondition('entity_id', $data['wInvId'])
      ->execute();
    if (!isset($result['payment'])) {
      watchdog('WayToPay', 'Missing invoice ID. POST data: <pre>!data</pre>', array('!data' => print_r($data,TRUE)), WATCHDOG_WARNING);
      return array('type' => 'ERROR', 'result' => 'Missing invoice ID.');
    }

    $payment = entity_load_single('payment', $data['wInvId']);

    if ($data['wSignature'] != self::signature($payment->method, $data)) {
      watchdog('WayToPay', 'Missing signature. POST data: <pre>!data</pre>', array('!data' => print_r($data,TRUE)), WATCHDOG_WARNING);
      return array('type' => 'ERROR', 'result' => 'Missing signature.');
    }

    if ($payment->getStatus()->status == PAYMENT_STATUS_SUCCESS) {
      watchdog('WayToPay', 'Payment already processed. POST data: <pre>!data</pre>', array('!data' => print_r($data,TRUE)), WATCHDOG_WARNING);
      return array('type' => 'ERROR', 'result' => 'Payment already processed.');
    }

    return array(
      'type' => 'OK',
      'result' => $data['wInvId'],
    );
  }
} 