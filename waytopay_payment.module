<?php

/**
 * @file
 * Hook implementations and shared functions.
 */

/**
 * Implements hook_menu().
 */
function waytopay_payment_menu() {
  $items['waytopay/redirect/%entity_object'] = array(
    'load arguments' => array('payment'),
    'title' => 'Go to payment server',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('waytopay_payment_form_redirect', 2),
    'access callback' => 'waytopay_payment_form_redirect_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );
  $items['waytopay/result'] = array(
    'page callback' => 'waytopay_payment_result',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['waytopay/%'] = array(
    'page callback' => 'waytopay_payment_status',
    'access callback' => 'waytopay_payment_status_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function waytopay_payment_status_access($status) {
  $data = $_POST;
  return in_array($status, array('success', 'fail')) && isset($data['wInvId']);
}

function waytopay_payment_status() {
  $data = $_POST;
  $payment = entity_load_single('payment', $data['wInvId']);
  $payment->finish();
}

/**
 * Form build callback: the redirect form.
 *
 * @param array $form
 * @param array $form_state
 * @param Payment $payment
 *
 * @return array
 */
function waytopay_payment_form_redirect(array $form, array &$form_state, Payment $payment) {
  $controller_data = $payment->method->controller_data;
  $data = array(
    'MerchantId' => $controller_data['merchant_id'],
    'OutSum' => $payment->totalAmount(TRUE),
    'InvId' => $payment->pid,
    'InvDesc' => $payment->description,
    'IncCurr' => $controller_data['inc_curr'],
  );

  // Build the form.
  $form['#action'] = url($controller_data['action_url'], array(
    'external' => TRUE,
  ));
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }
  }
  $form['message'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('You will be redirected to the off-site payment server to authorize the payment.') . '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Access callback for the redirect page.
 *
 * @param Payment $payment
 *   The payment to check access to.
 * @param object $user
 *   An optional user to check access for. If NULL, then the currently logged
 *   in user is used.
 *
 * @return bool
 */
function waytopay_payment_form_redirect_access(Payment $payment, $account = NULL) {
  if (!$account) {
    global $user;
    $account = $user;
  }
  return $account->uid == $payment->uid &&
  is_a($payment->method->controller, 'PaymentMethodWayToPayController')
  && payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_PENDING);
}

/**
 * Processes an result request based on POST data.
 */
function waytopay_payment_result() {
  $data = $_POST;
  $result = PaymentMethodWayToPayController::validate_post($data);
  if ($result['type'] == 'OK') {
    $payment = entity_load_single('payment', $data['wInvId']);
    $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
    entity_save('payment',$payment);
  }
  print implode('_', $result);
  drupal_exit();
}

/**
 * Implements hook_payment_method_controller_info().
 */
function waytopay_payment_payment_method_controller_info() {
  return array('PaymentMethodWayToPayController');
}

/**
 * Implements hook_entity_load().
 */
function waytopay_payment_entity_load(array $entities, $entity_type) {
  if ($entity_type == 'payment_method') {
    $pmids = array();
    foreach ($entities as $payment_method) {
      if ($payment_method->controller->name == 'PaymentMethodWayToPayController') {
        $pmids[] = $payment_method->pmid;
      }
    }
    if ($pmids) {
      $query = db_select('payment_method_waytopay')
        ->fields('payment_method_waytopay')
        ->condition('pmid', $pmids);
      $result = $query->execute();
      while ($data = $result->fetchAssoc()) {
        $payment_method = $entities[$data['pmid']];
        $payment_method->controller_data = (array) $data;
        unset($payment_method->controller_data['pmid']);
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function waytopay_payment_payment_method_insert(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'PaymentMethodWayToPayController') {
    $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $query = db_insert('payment_method_waytopay');
    $values = array_merge($payment_method->controller_data, array(
      'pmid' => $payment_method->pmid,
    ));
    $query->fields($values);
    $query->execute();
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function waytopay_payment_payment_method_update(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'PaymentMethodWayToPayController') {
    $query = db_update('payment_method_waytopay');
    $values = array_merge($payment_method->controller_data, array(
      'pmid' => $payment_method->pmid,
    ));
    $query->fields($values);
    $query->condition('pmid', $payment_method->pmid);
    $query->execute();
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function waytopay_payment_payment_method_delete($entity) {
  if ($entity->controller->name == 'PaymentMethodWayToPayController') {
    db_delete('payment_method_waytopay')
      ->condition('pmid', $entity->pmid)
      ->execute();
  }
}

/**
 * Form build callback: implements
 * PaymentMethodController::payment_method_configuration_form_elements_callback.
 *
 * @return array
 *   A Drupal form.
 */
function waytopay_payment_method_configuration_form_elements(array $form, array &$form_state) {
  $payment_method = $form_state['payment_method'];
  $controller = $payment_method->controller;
  $controller_data = $payment_method->controller_data + $controller->controller_data_defaults;

  $elements['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Id'),
    '#default_value' => isset($controller_data['merchant_id']) ? $controller_data['merchant_id'] : '',
    '#required' => TRUE,
  );
  $elements['secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret_key'),
    '#default_value' => isset($controller_data['secret_key']) ? $controller_data['secret_key'] : '',
    '#required' => TRUE,
  );
  $elements['action_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Action URL'),
    '#default_value' => isset($controller_data['action_url']) ? $controller_data['action_url'] : 'https://waytopay.org/merchant/index',
    '#required' => TRUE,
  );

  $elements['inc_curr'] = array(
    '#type' => 'select',
    '#title' => t('Payment mathod'),
    '#empty_value' => 0,
    '#default_value' => isset($controller_data['inc_curr']) ? $controller_data['inc_curr'] : 0,
    '#options' => PaymentMethodWayToPayController::$payways,
  );
  $elements['status'] = array(
    '#type' => 'select',
    '#title' => t('Final payment status'),
    '#description' => t('The status to give a payment after being processed by this payment method.'),
    '#default_value' => isset($controller_data['status']) ? $controller_data['status'] : PAYMENT_STATUS_SUCCESS,
    '#options' => payment_status_options(),
  );
  $elements['message'] = array(
    '#type' => 'text_format',
    '#title' => t('Payment form message'),
    '#default_value' => isset($controller_data['message']) ? $controller_data['message'] : '',
    '#format' => isset($controller_data['text_format']) ? $controller_data['text_format'] : filter_default_format(),
  );

  return $elements;
}

/**
 * Implements form validate callback for
 * paymentmethodbasic_payment_method_configuration_form_elements().
 */
function waytopay_payment_method_configuration_form_elements_validate(array $element, array &$form_state) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  $form_state['payment_method']->controller_data['merchant_id'] = $values['merchant_id'];
  $form_state['payment_method']->controller_data['secret_key'] = $values['secret_key'];
  $form_state['payment_method']->controller_data['action_url'] = $values['action_url'];
  $form_state['payment_method']->controller_data['inc_curr'] = $values['inc_curr'];
  if (!valid_url($values['action_url'], TRUE)) {
    form_error($element['action_url'], t('The Action URL is not valid.'));
  }
  $form_state['payment_method']->controller_data['status'] = $values['status'];
  $form_state['payment_method']->controller_data['message'] = $values['message']['value'];
  $form_state['payment_method']->controller_data['text_format'] = $values['message']['format'];
}

/**
 * Implements
 * PaymentMethodController::payment_configuration_form_elements_callback.
 */
function waytopay_payment_configuration_form_elements(array $element, array &$form_state) {
  $payment = $form_state['payment'];

  $elements = array();
  $elements['message'] = array(
    '#type' => 'markup',
    '#markup' => check_markup($payment->method->controller_data['message'], $payment->method->controller_data['text_format']),
  );

  return $elements;
}